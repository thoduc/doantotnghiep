<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/17/2017
 * Time: 8:50 AM
 */

namespace App\Http\Controllers;

use App\Entity\Author;
use App\Entity\AuthorPaper;
use App\Entity\Candidate;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class CandidateController extends LayoutController
{
    public function index() {
        
        return view('candidate.index');
    }

    public function anyDatabase() {
        $candidate = new Candidate();
        $candidates = $candidate
            ->join('author as au1', 'au1.id', '=', 'candidate.author_1')
            ->join('author as au2', 'au2.id', '=', 'candidate.author_2')
            ->select(
                'candidate.candidate_id',
                'au1.givenName as givenName_1',
                'au2.givenName as givenName_2',
                'au1.surname as surname_1',
                'au2.surname as surname_2',
                'au1.affiliation as affiliation_1',
                'au2.affiliation as affiliation_2',
                'candidate.point'
            );

        return Datatables::of($candidates)
            ->addColumn('fullName_1', function($candidate) {
                return $candidate->givenName_1.' ('.$candidate->surname_1.')';
            })
            ->addColumn('fullName_2', function($candidate) {
                return $candidate->givenName_2.' ('.$candidate->surname_2.')';
            })
            ->make(true);
    }
    
    public function computeCandidate(Request $request) {
        $start = $request->input('start');

        $limit = 50;
        if ($start == 0) {
            $candidate = new Candidate();
            $candidate->truncate();
        }
        $countAuthor = Author::orderBy('author_id', 'asc')->count();
        //$countAuthor =  200;

        $authors = Author::orderBy('author_id', 'asc')->select('id as authorid')->offset($start)
            ->limit($limit)->get();
        // không còn tác giả nào cả
        if (empty($authors) || ($start >= $countAuthor) )  {
            return response([
                'status' => 200,
                'success' =>  1,
                'percent' => 100
            ])->header('Content-Type', 'text/plain');
        }
        $authorColleagues = AuthorPaper::join('author_paper as ap1', 'ap1.paperid', '=', 'author_paper.paperid')->
        whereIn('author_paper.authorid',$authors)
            ->select(
                'author_paper.authorid as mainAuthor',
                'ap1.authorid as colleagueAuthor'
            )                           
            ->get();
        $colleagueOneAuthor = array();
        $colleagues = array();
        foreach ($authorColleagues as $colleague) {
            if (!isset($colleagueOneAuthor[$colleague->mainAuthor])
                || in_array($colleague->colleagueAuthor, $colleagueOneAuthor[$colleague->mainAuthor]) == false) {
                $colleagueOneAuthor[$colleague->mainAuthor][] = $colleague->colleagueAuthor;
                $colleagues[] = $colleague->colleagueAuthor;
            }
        }

        $doubleCollecgues = AuthorPaper::join('author_paper as ap1', 'ap1.paperid', '=', 'author_paper.paperid')->
        whereIn('author_paper.authorid',$colleagues)
            ->select(
                'author_paper.authorid as mainAuthor',
                'ap1.authorid as colleagueAuthor'
            )
            ->get();
        $colleagueOfColleagues = array();
        foreach ($doubleCollecgues as $colleague) {
            if (!isset($colleagueOfColleagues[$colleague->mainAuthor])
                || in_array($colleague->colleagueAuthor, $colleagueOfColleagues[$colleague->mainAuthor]) == false) {
                $colleagueOfColleagues[$colleague->mainAuthor][] = $colleague->colleagueAuthor;
            }
         }


        foreach ($authors as $id => $author) {
            $dataInsert = array();
            foreach ($colleagueOfColleagues as $colleagueAuthor => $colleague) {
                if ($colleagueAuthor != $author->authorid) {
                    $linkPoint = count(array_intersect($colleagueOneAuthor[$author->authorid], $colleague));
                    if ($linkPoint > 0 ) {
                        $dataInsert[] = [
                            'author_1' => $author->authorid,
                            'author_2'=> $colleagueAuthor,
                            'point' => $linkPoint,
                        ];
                    }
                }
            }
            foreach ($authors as $idAfter => $authorNetwork) {
                if ($idAfter > $id) {
                    $linkPoint = count(array_intersect($colleagueOneAuthor[$author->authorid], $colleagueOneAuthor[$authorNetwork->authorid]));
                    if ($linkPoint > 0 ) {
                        $dataInsert[] = [
                            'author_1' => $author->authorid,
                            'author_2'=>  $authorNetwork->authorid,
                            'point' => $linkPoint,
                        ];
                    }
                }
            }

            $authorNetwork = new Candidate();
            $authorNetwork->insert($dataInsert);
        }

        return response([
            'status' => 200,
            'success' => 0,
            'percent' => ($start/$countAuthor)*100
        ])->header('Content-Type', 'text/plain');
    }
}
