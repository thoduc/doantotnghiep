<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Paper extends Model
{
    protected $table = 'paper';

    protected $primaryKey = 'document_id';
    
    protected $fillable = [
        'document_id',
        'id',
        'coverDate',
        'abstract',
        'title',
        'url',
        'issn',
        'keywords',
        'created_at',
        'updated_at'
    ];
}
