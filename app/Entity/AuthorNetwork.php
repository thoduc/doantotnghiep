<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/17/2017
 * Time: 8:57 AM
 */

namespace App\Entity;
use Illuminate\Database\Eloquent\Model;

class AuthorNetwork extends Model
{
    protected $table = 'author_network';

    protected $primaryKey = 'author_network_id';

    protected $fillable = [
        'author_network_id',
        'author_1',
        'author_2',
        'link_point',
        'created_at',
        'updated_at',
    ];
}
