@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới tài liệu
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('papers.index') }}">Tài liệu</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('papers.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Chọn tên tác giả</label>
                                <select name="authors[]" class="form-control" id="author_list" multiple="multiple">

                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã tài liệu</label>
                                <input type="text" class="form-control" name="id" placeholder="Mã tài liệu" required/>
                            </div>
                            
                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề</label>
                                <input type="text" class="form-control" name="title" placeholder="Tiêu đề" />
                            </div>

                            <div class="form-group">
                                <label>Ngày xuất bản:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="coverDate" class="form-control pull-right" id="datepicker">
                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Link trên trang khoa học</label>
                                <input type="text" class="form-control" name="url" placeholder="Đường dẫn..." >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">ID tạp chí</label>
                                <input type="text" class="form-control" name="issn" placeholder="ID tạp chí">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Từ khóa</label>
                                <input type="text" class="form-control" name="keywords" placeholder="Từ khóa" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Abstract</label>
                                <input type="text" class="form-control" name="abstract" placeholder="Abstract" />
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('title'))
                                    <label for="exampleInputEmail1">{{ $errors->first('title') }}</label>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                
            </form>
        </div>
    </section>
@endsection

