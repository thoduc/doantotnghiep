<header class="main-header">
    <!-- Logo -->
    <a href="{{ route('home') }}" class="logo">
        <!-- mini logo for sidebar mini 50x50 pixels -->
        <span class="logo-mini"><b>A</b>LT</span>
        <!-- logo for regular state and mobile devices -->
        <span class="logo-lg"><b>Quản trị</b> website</span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="push-menu" role="button">
            <span class="sr-only">Toggle navigation</span>
        </a>

        <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                @if (!Auth::check())
                <li>
                    <a href="{{ route('login') }}">Đăng nhập</a>
                </li>
                @endif
                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                        <img src="{{ Auth::check() ?  asset(Auth::user()->image) :  asset('/images/256.png')  }}" class="user-image" alt="User Image">
                        <span class="hidden-xs">{{  Auth::check() ? Auth::user()->name : 'Khách'}}</span>
                    </a>
                    <ul class="dropdown-menu">
                        <!-- User image -->
                        <li class="user-header">
                            <img src="{{ Auth::check() ? asset(Auth::user()->image): '' }}" class="img-circle" alt="User Image">

                            <p>
                                {{ Auth::check() ? Auth::user()->name : 'Khách' }}
                                <small>{{ Auth::check() ? Auth::user()->email : 'Khách' }}</small>
                                <small>{{Auth::check() ? Auth::user()->phone : 'no phone'}}</small>
                            </p>
                        </li>
                        @if(Auth::check())
                        <!-- Menu Footer-->
                        <li class="user-footer">
                            <div class="pull-left">
                                <a href="#" class="btn btn-default btn-flat">Thông tin</a>
                            </div>

                            <div class="pull-right">
                                <a class="btn btn-default btn-flat" onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">Đăng suất</a>
                                <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                            </div>
                        </li>
                        @endif
                    </ul>
                </li>
                <!-- Control Sidebar Toggle Button -->
            </ul>
        </div>
    </nav>
</header>
