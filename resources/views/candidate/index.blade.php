@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tìm kiếm ứng viên
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i>Home</a></li>
            <li><a href="#">Danh sách ứng viên</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @if (Auth::check())
                        <button class="btn btn-primary" onclick="return computeCandidate(this);">Cập nhật ứng viên</button>
                        <div class="progress" sytle="display: none">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" >
                                0%
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="papers" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="20%">Tác giả 1</th>
                                <th width="20%">Địa chỉ tác giả 1</th>
                                <th width="20%">Tác giả 2</th>
                                <th width="20%">Địa chỉ tác giả 2</th>
                                <th>Chỉ số ứng viên</th>
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
    @include('partials.popup_delete')
@endsection

@push('scripts')
<script>
    $(function() {
        $('#papers').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('candidate.anyData') !!}',
            columns: [
                { data: 'candidate_id', name: 'candidate_id' },
                { data: 'fullName_1', name: 'au1.givenName' },
                { data: 'affiliation_1', name: 'au1.affiliation' },
                { data: 'fullName_2', name: 'au2.givenName' },
                { data: 'affiliation_2', name: 'au2.affiliation' },
                { data: 'point', name: 'point' },
            ]
//            order: [[3, 'desc']]
        });
    });


    function computeCandidate(e) {
        var btn = $(e).button('loading');
        $('.progress').show();
        var start = 0;
        update();
        function update() {
            $.ajax({
                type: "get",
                url: '{!! route('candidate.update') !!}',
                data: {
                    start: start,
                },
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    $('.progress-bar').attr('style', 'width: '+obj.percent+'%;');
                    $('.progress-bar').empty();
                    $('.progress-bar').append(obj.percent+'%');
                    console.log(start);
                    start += 50;
                    if (obj.success == 1) {
                        $('.progress-bar').attr('style', 'width: 100%;');
                        $('.progress-bar').empty();
                        $('.progress-bar').append('100%');
                        alert('Bạn đã cập nhật thành công');
                        btn.button('reset')
                        $('.progress').hide();
                        location.reload();
                    } else {
                        update();
                    }
                }
            });
        }
        
    }


</script>
@endpush


