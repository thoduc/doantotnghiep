<?php

namespace App\Http\Controllers;

use App\Entity\Author;
use App\Entity\AuthorOffice;
use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;

class AuthorController extends LayoutController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('author.list');
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('author.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'givenName' => 'required',
            'email' => 'required|email',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('authors/create')
                ->withErrors($validation)
                ->withInput();
        }

        $author = Author();
        $author->insert([
            'givenName' => $request->input('givenName'),
            'surname' => $request->input('surname'),
            'email' => $request->input('email'),
            'affiliation' => $request->input('affiliation'),
            'subjects' => $request->input('subjects'),
            'url' => $request->input('url'),
            'created_at' => new \Datetime(),
            'id' => $request->input('id'),

        ]);

        return redirect(route('authors.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function show(Author $author)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function edit(Author $author)
    {
        return view('author.edit', compact('author'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Author $author)
    {
        $validation = Validator::make($request->all(), [
            'givenName' => 'required',
            'email' => 'required|email',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('authors.edit', ['author_id' => $author->author_id]))
                ->withErrors($validation)
                ->withInput();
        }

        $author->update([
            'givenName' => $request->input('givenName'),
            'surname' => $request->input('surname'),
            'email' => $request->input('email'),
            'affiliation' => $request->input('affiliation'),
            'subjects' => $request->input('subjects'),
            'url' => $request->input('url'),
            'created_at' => new \Datetime(),
            'id' => $request->input('id'),

        ]);

        return redirect(route('authors.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Author  $author
     * @return \Illuminate\Http\Response
     */
    public function destroy(Author $author)
    {
        $author->delete();

        return redirect(route('authors.index'));
    }

    public function anyDatatables() {
        $author = new Author();
        $authors = $author->select('*');
        
        return Datatables::of($authors)
            ->addColumn('action', function($author) {
                if (!Auth::check()) {
                    return '';
                }
                $string =  '<a href="'.route('authors.edit', ['author_id' => $author->author_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('authors.destroy', ['author_id' => $author->author_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->addColumn('title_show', function($author) {
                $string = '<a href="'.$author->url.'">'.$author->surname.'('.$author->givenName.')</a>';

                return $string;
            })
            ->rawColumns(['action', 'title_show'])
            ->make(true);
    }

    public function apiSelect(Request $request) {
        $input = $request->input('search');
        $authors = Author::where('givenName', 'like', $input.'%')
            ->orWhere('givenName', 'like', '%'.$input.'%')
            ->orWhere('surname', 'like', '% '.$input.'%')
            ->orWhere('surname', 'like', $input.'%')
            ->get( ['givenName AS text', 'id'])->toArray();
        
        if (!empty($input))
        {
            return response()->json([
                'results' => $authors
            ])->header('Content-Type', 'text/plain');
        }
        else
        {
            return response()->json([
                'results' => ''
            ])->header('Content-Type', 'text/plain');
        }
    }

    public function computeAuthorOffice(Request $request) {
        $start = $request->input('start');
        if ($start == 0) {
            $authorOffice = new AuthorOffice();
            $authorOffice->truncate();
        }
        $limit = 100;
        // Số lượng tác giả chạy
        //$countPaper =  Author::orderBy('author_id', 'asc')->count();
        $countAuthor = 200;

        $authors =  Author::orderBy('author_id', 'asc')->offset($start)
            ->limit($limit)->get();
        
        if (empty($authors) || ($start >= $countAuthor) ) {
            return response([
                'status' => 200,
                'success' =>  1,
                'percent' => 100
            ])->header('Content-Type', 'text/plain');
        }

        $insertData = array();
        foreach ($authors as $author) {
            $areas = explode(',', $author->affiliation);
            if (count($areas) == 3) {
                $insertData[] = [
                    'author_id' => $author->id,
                    'university' =>  $areas[0],
                    'department' =>  '',
                    'province' => $areas[1],
                    'country' =>  $areas[2],
                ];
            }

            if (count($areas) == 4) {
                $insertData[] = [
                    'author_id' => $author->id,
                    'university' =>  $areas[0],
                    'department' => $areas[1] ,
                    'province' => $areas[2],
                    'country' =>  $areas[3],
                ];
            }

            if (count($areas) == 5) {
                $insertData[] = [
                    'author_id' => $author->id,
                    'university' =>  $areas[0],
                    'department' => $areas[1].', '.$areas[2] ,
                    'province' => $areas[3],
                    'country' =>  $areas[4],
                ];
            }
        }

        $authorOffice = new AuthorOffice();
        $authorOffice->insert($insertData);

        return response([
            'status' => 200,
            'success' => 0,
            'percent' => ($start/$countAuthor)*100
        ])->header('Content-Type', 'text/plain');

    }
}
