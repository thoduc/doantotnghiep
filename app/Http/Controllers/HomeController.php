<?php

namespace App\Http\Controllers;

use App\Entity\Author;
use App\Entity\Paper;
use App\Entity\User;
use Illuminate\Http\Request;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
       // $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $countAuthor = Author::count();
        $countPaper = Paper::count();
        $countUser = User::count();

        return View('home', compact(
            'countAuthor',
            'countPaper',
            'countUser'
        ));
    }
}
