<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{ Auth::check() ? asset(Auth::user()->image): asset('/images/256.png') }}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>{{ Auth::check() ? Auth::user()->name : 'Khách' }}</p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        @if(Auth::check() && !\App\Entity\User::isMember(\Illuminate\Support\Facades\Auth::user()->role))
        <!-- sidebar menu: : style can be found in sidebar.less -->
            <ul class="sidebar-menu" data-widget="tree">
                <li class="header">Nội dung</li>
                <li class="{{ Request::is('authors', 'authors/create') ? 'active' : null }} treeview">
                    <a href="{{ route('authors.index') }}">
                        <i class="fa fa-user" aria-hidden="true"></i> <span>Quản lý tác giả</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::is( 'authors' ) ? 'active' : null }}">
                            <a href="{{ route('authors.index') }}"><i class="fa fa-circle-o"></i>Tất cả tác giả</a>
                        </li>
                        <li class="{{ Request::is('authors/cre
                        ate') ? 'active' : null }}">
                            <a href="{{ route('authors.create') }}"><i class="fa fa-circle-o"></i>Thêm mới tác giả</a>
                        </li>
                    </ul>
                </li>

                <li class="{{ Request::is('papers', 'papers/create') ? 'active' : null }} treeview">
                    <a href="{{ route('papers.index') }}">
                        <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Quản lý tài liệu</span>
                        <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{ Request::is( 'papers' ) ? 'active' : null }}">
                            <a href="{{ route('papers.index') }}"><i class="fa fa-circle-o"></i>Tất cả tài liệu</a>
                        </li>
                        <li class="{{ Request::is('papers/cre
                        ate') ? 'active' : null }}">
                            <a href="{{ route('papers.create') }}"><i class="fa fa-circle-o"></i>Thêm mới tài liệu</a>
                        </li>
                    </ul>
                </li>
                <li class="{{ Request::is( route('author_network.index')) ? 'active' : null }} ">
                    <a href="{{ route('author_network.index') }}">
                        <i class="fa fa-link" aria-hidden="true"></i> <span>Quản lý mạng tác giả</span>
                    </a>
                </li>
                <li class="{{ Request::is( route('candidate.index')) ? 'active' : null }} ">
                    <a href="{{ route('candidate.index') }}">
                        <i class="fa fa-search" aria-hidden="true"></i> <span>Tìm kiếm ứng viên</span>
                    </a>
                </li>

                @if(\App\Entity\User::isManager(\Illuminate\Support\Facades\Auth::user()->role))
                    <li class="header">Thành viên</li>
                    <li class="{{ Request::is('admin/users', 'admin/users/create') ? 'active' : null }} treeview">
                        <a href="{{ route('users.index') }}">
                            <i class="fa fa-wrench" aria-hidden="true"></i> <span>Quản lý thành viên</span>
                            <span class="pull-right-container">
                      <i class="fa fa-angle-left pull-right"></i>
                    </span>
                        </a>
                        <ul class="treeview-menu">
                            <li class="{{ Request::is( 'admin/users' ) ? 'active' : null }}">
                                <a href="{{ route('users.index') }}"><i class="fa fa-circle-o"></i>Tất cả Thành viên</a>
                            </li>
                            <li class="{{ Request::is('admin/users/create') ? 'active' : null }}">
                                <a href="{{ route('users.create') }}"><i class="fa fa-circle-o"></i>Thêm mới thành viên</a>
                            </li>
                        </ul>
                    </li>
                @endif
            </ul>
            @else
                <ul class="sidebar-menu" data-widget="tree">
                    <li class="header">Nội dung</li>
                    <li class="{{ Request::is(route('authors.customer') ) ? 'active' : null }} ">
                        <a href="{{ route('authors.customer') }}">
                            <i class="fa fa-user" aria-hidden="true"></i> <span>Tra cứu tác giả</span>
                        </a>
                    </li>

                    <li class="{{ Request::is(route('papers.customer') ) ? 'active' : null }} ">
                        <a href="{{ route('papers.customer') }}">
                            <i class="fa fa-newspaper-o" aria-hidden="true"></i> <span>Tra cứu tài liệu</span>
                        </a>
                    </li>
                    <li class="{{ Request::is( route('author_network.customer')) ? 'active' : null }} ">
                        <a href="{{ route('author_network.customer') }}">
                            <i class="fa fa-link" aria-hidden="true"></i> <span>Tra cứu mạng tác giả</span>
                        </a>
                    </li>
                    <li class="{{ Request::is( route('candidate.customer')) ? 'active' : null }} ">
                        <a href="{{ route('candidate.customer') }}">
                            <i class="fa fa-search" aria-hidden="true"></i> <span>Tìm kiếm ứng viên</span>
                        </a>
                    </li>
                </ul>

        @endif
    </section>
    <!-- /.sidebar -->
</aside>
