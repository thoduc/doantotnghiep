<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/17/2017
 * Time: 8:49 AM
 */

namespace App\Http\Controllers;

use App\Entity\Author;
use App\Entity\AuthorNetwork;
use App\Entity\AuthorPaper;
use Illuminate\Support\Facades\DB;
use Yajra\Datatables\Datatables;
use Illuminate\Http\Request;

class AuthorNetworkController extends LayoutController
{
    public function index() {
        return view('authorNetwork.index');
    }

    public function anyDatabase() {
        $authorNetwork = new AuthorNetwork();
        $authorNetworks = $authorNetwork
            ->join('author as au1', 'au1.id', '=', 'author_network.author_1')
            ->join('author as au2', 'au2.id', '=', 'author_network.author_2')
            ->select(
                'author_network.author_network_id',
                'au1.givenName as givenName_1',
                'au2.givenName as givenName_2',
                'au1.surname as surname_1',
                'au2.surname as surname_2',
                'author_network.link_point',
                'au1.affiliation as affiliation_1',
                'au2.affiliation as affiliation_2'
            );

        return Datatables::of($authorNetworks)
            ->addColumn('fullName_1', function($authorNetwork) {
                return $authorNetwork->givenName_1.' ('.$authorNetwork->surname_1.')';
            })
            ->addColumn('fullName_2', function($authorNetwork) {
                return $authorNetwork->givenName_2.' ('.$authorNetwork->surname_2.')';
            })
            ->make(true);
    }
    
    public function computeNetwork(Request $request) {
        $start = $request->input('start');
        $limit = 100;
        if ($start == 0) {
            $authorNetwork = new AuthorNetwork();
            $authorNetwork->truncate();
        }
        
        $countAuthor = Author::orderBy('author_id', 'asc')->count();
        //$countAuthor =  300;
        
        $authors = Author::orderBy('author_id', 'asc')->select('id as authorid')->offset($start)
            ->limit($limit)->get();
        // không còn tác giả nào cả
        if (empty($authors) || ($start >= $countAuthor) )  {
            return response([
                'status' => 200,
                'success' =>  1,
                'percent' => 100
            ])->header('Content-Type', 'text/plain');
        }
        $documentAuthor = array();
        $documentSearchs = array();
        $documentOfAuthors = AuthorPaper::whereIn('authorid',$authors)->select('paperid', 'authorid')->get();
        foreach ($documentOfAuthors as $document) {
            $documentAuthor[$document->authorid][] = $document->paperid;
            $documentSearchs[] = $document->paperid;
        }
        $authorNetworks = AuthorPaper::whereIn('paperid', $documentSearchs)
            ->whereNotIn('authorid', $authors)
            ->select('authorid', 'paperid')
            ->distinct()
            ->get();

        foreach($authorNetworks as $authorNetwork) {
            $documentAuthor[$authorNetwork->authorid][] = $authorNetwork->paperid;
        }

        foreach ($authors as $id => $author) {
            $dataInsert = array();
            foreach ($documentAuthor as $authorNetwork => $document) {
                 if ($authorNetwork != $author->authorid) {
                     $linkPoint = count(array_intersect($documentAuthor[$author->authorid], $document));
                     if ($linkPoint > 0 ) {
                         $dataInsert[] = [
                             'author_1' => $author->authorid,
                             'author_2'=>  $authorNetwork,
                             'link_point' => $linkPoint,
                         ];
                     }
                 }
            }
            foreach ($authors as $idAfter => $authorNetwork) {
                if ($idAfter > $id) {
                    $linkPoint = count(array_intersect($documentAuthor[$author->authorid], $documentAuthor[$authorNetwork->authorid]));
                    if ($linkPoint > 0 ) {
                        $dataInsert[] = [
                            'author_1' => $author->authorid,
                            'author_2'=>  $authorNetwork->authorid,
                            'link_point' => $linkPoint,
                        ];
                    }
                }
            }

            $authorNetwork = new AuthorNetwork();
            $authorNetwork->insert($dataInsert);
        }
        
        return response([
            'status' => 200,
            'success' => 0,
            'percent' => ($start/$countAuthor)*100
        ])->header('Content-Type', 'text/plain');

    }
}
