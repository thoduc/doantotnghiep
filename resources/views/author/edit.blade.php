@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa tác giả {{$author->givenName}} ({{$author->surname}})
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('authors.index') }}">Tác giả</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('authors.update', ['author_id' => $author->author_id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-8">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">
                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã tác giả</label>
                                <input type="text" class="form-control" value="{{ $author->id }}" name="id" placeholder="Mã tác giả" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Trường/viện nơi họ làm việc</label>
                                <input type="text" class="form-control" value="{{ $author->affiliation }}" name="affiliation" placeholder="Trường/viện nơi họ làm việc" >
                            </div>


                            <div class="form-group">
                                <label for="exampleInputEmail1">Lĩnh vực chuyên môn</label>
                                <input type="text" class="form-control"  value="{{ $author->subjects }}" name="subjects" placeholder="Lĩnh vực chuyên môn" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên chính thức</label>
                                <input type="text" class="form-control" value="{{ $author->givenName }}" name="givenName" placeholder="Tên chính thức" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Ký danh</label>
                                <input type="text" class="form-control" value="{{ $author->surname }}" name="surname" placeholder="Ký danh" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" value="{{ $author->email }}" name="email" placeholder="Email" required />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Trang cá nhân</label>
                                <input type="text" class="form-control" value="{{ $author->url }}" name="url" placeholder="Trang cá nhân" >
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('email'))
                                    <label for="exampleInputEmail1">{{ $errors->first('email') }}</label>
                                @endif
                                @if ($errors->has('givenName'))
                                    <label for="exampleInputEmail1">{{ $errors->first('givenName') }}</label>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Thay đổi</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

