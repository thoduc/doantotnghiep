<?php

namespace App\Http\Controllers;

use App\Entity\AuthorPaper;
use App\Entity\Paper;
use App\Entity\PaperKeyword;
use App\Entity\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Yajra\Datatables\Datatables;
use Illuminate\Validation\Rule;
use Validator;

class PaperController extends LayoutController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('document.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
         return view('document.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect('papers/create')
                ->withErrors($validation)
                ->withInput();
        }

        $authors = $request->input('authors');

        $paper = new Paper();
        $paperId = $paper->insertGetId([
            'id' => $request->input('id'),
            'title' => $request->input('title'),
            'coverDate' => new \Datetime($request->input('coverDate')),
            'url' => $request->input('url'),
            'issn' => $request->input('issn'),
            'keywords' => $request->input('keywords'),
            'abstract' => $request->input('abstract'),
        ]);


        foreach($authors as $author) {
            AuthorPaper::insert([
                'paperid' => $request->input('id'),
                'authorid' => $author
            ]);
        }

        return redirect(route('papers.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Entity\Paper  $paper
     * @return \Illuminate\Http\Response
     */
    public function show(Paper $paper)
    {

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Entity\Paper  $paper
     * @return \Illuminate\Http\Response
     */
    public function edit(Paper $paper)
    {
        $authorPaper = new AuthorPaper();
        $authors = $authorPaper->join('author', 'author.id', '=', 'author_paper.authorid')
            ->select('author.givenName', 'author.id')
            ->where('author_paper.paperid', '=', $paper->id)
            ->get();

         return view('document.edit', compact('paper', 'authors'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Entity\Paper  $paper
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Paper $paper)
    {
        $validation = Validator::make($request->all(), [
            'title' => 'required',
        ]);

        // if validation fail return error
        if ($validation->fails()) {
            return redirect(route('papers.edit', ['document_id' => $paper->document_id]))
                ->withErrors($validation)
                ->withInput();
        }

        $authors = $request->input('authors');
        $paper->update([
            'title' => $request->input('title'),
            'coverDate' => new \Datetime($request->input('coverDate')),
            'url' => $request->input('url'),
            'issn' => $request->input('issn'),
            'keywords' => $request->input('keywords'),
            'abstract' => $request->input('abstract'),
        ]);

        AuthorPaper::where('paperid', $paper->id)->delete();
        foreach($authors as $author) {
            AuthorPaper::insert([
                'paperid' => $paper->id,
                'authorid' => $author
            ]);
        }
        return redirect(route('papers.index'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Entity\Paper  $paper
     * @return \Illuminate\Http\Response
     */
    public function destroy(Paper $paper)
    {
        $paper->delete();
        AuthorPaper::where('paperid', $paper->id)->delete();
        
        return redirect(route('papers.index'));
    }

    public function anyDatatables() {
        $paper = new Paper();
        $papers = $paper->select('*');

        return Datatables::of($papers)
            ->addColumn('author', function ($paper) {
                $authorPaper = new AuthorPaper();
                $authors = $authorPaper->join('author', 'author.id', '=', 'author_paper.authorid')
                    ->select('author.givenName', 'author.surname')
                    ->where('author_paper.paperid', '=', $paper->id)
                    ->get();
                $string = '';
                foreach($authors as $author) {
                    if (empty($string)) {
                        $string = $author->givenName.' ('.$author->surname.')';
                        continue;
                    }
                    $string = implode(',', [$string, $author->givenName.' ('.$author->surname.')']);
                }

                return $string;
            })
            ->addColumn('title_show', function ($paper) {
                $string = '<a href="'.$paper->url.'">'.$paper->title.'</a>';

                return $string;
            } )
            ->addColumn('action', function($paper) {
                if (!Auth::check()) {
                    return '';
                }
                $string =  '<a href="'.route('papers.edit', ['document_id' => $paper->document_id]).'">
                           <button class="btn btn-primary"><i class="fa fa-pencil" aria-hidden="true"></i></button>
                       </a>';
                $string .= '<a  href="'.route('papers.destroy', ['document_id' => $paper->document_id]).'" class="btn btn-danger btnDelete" 
                            data-toggle="modal" data-target="#myModalDelete" onclick="return submitDelete(this);">
                               <i class="fa fa-trash-o" aria-hidden="true"></i>
                            </a>';
                return $string;
            })
            ->rawColumns(['author', 'title_show', 'action'])
            ->make(true);
    }

    public function computeKeyword(Request $request) {
        $start = $request->input('start');
        if ($start == 0) {
            $paperKeyword = new PaperKeyword();
            $paperKeyword->truncate();
        }
        
        $limit = 100;
        $countPaper =  Paper::orderBy('document_id', 'asc')->count();

        $papers =  Paper::orderBy('document_id', 'asc')->offset($start)
            ->limit($limit)->get();

        if (empty($papers) || ($start >= $countPaper) ) {
            return response([
                'status' => 200,
                'success' =>  1,
                'percent' => 100
            ])->header('Content-Type', 'text/plain');
        }
        
        $insertData = array();
        foreach ($papers as $paper) {
            $keywords = explode(',', $paper->keywords);
            foreach ($keywords as $keyword) {
                $insertData[] = [
                    'paper_id' => $paper->id,
                    'keyword' =>  $keyword
                ];
            }
        }
        $paperKeyword = new PaperKeyword();
        $paperKeyword->insert($insertData);
        
        return response([
            'status' => 200,
            'success' => 0,
            'percent' => ($start/$countPaper)*100
        ])->header('Content-Type', 'text/plain');

    }
}
