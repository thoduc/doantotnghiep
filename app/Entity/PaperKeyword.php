<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/19/2017
 * Time: 1:54 PM
 */

namespace App\Entity;
use Illuminate\Database\Eloquent\Model;

class PaperKeyword extends Model
{
    protected $table = 'paper_keywords';

    protected $primaryKey = 'paper_keyword_id';

    protected $fillable = [
        'paper_keyword_id',
        'paper_id',
        'keyword',
        'created_at',
        'updated_at',
    ];
}
