<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/17/2017
 * Time: 8:57 AM
 */

namespace App\Entity;
use Illuminate\Database\Eloquent\Model;

class Candidate extends Model
{
    protected $table = 'candidate';

    protected $primaryKey = 'candidate_id';

    protected $fillable = [
        'candidate_id',
        'author_1',
        'author_2',
        'point',
        'created_at',
        'updated_at',
    ];
}
