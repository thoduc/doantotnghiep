@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Chỉnh sửa bài báo {{$paper->title}}
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('papers.index') }}">Bài báo</a></li>
            <li class="active">Chỉnh sửa</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('papers.update', ['document_id' => $paper->document_id]) }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('PUT') }}
                <div class="col-xs-12 col-md-8">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Chọn tên tác giả</label>
                                <select name="authors[]" class="form-control" id="author_list" multiple="multiple">
                                     @foreach($authors as $author)
                                        <option value="{{ $author->id }}" selected>{{ $author->givenName }}</option>
                                     @endforeach
                                </select>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tiêu đề</label>
                                <input type="text" class="form-control" name="title" value="{{ $paper->title }}" placeholder="Tiêu đề" />
                            </div>

                            <div class="form-group">
                                <label>Ngày xuất bản:</label>

                                <div class="input-group date">
                                    <div class="input-group-addon">
                                        <i class="fa fa-calendar"></i>
                                    </div>
                                    <input type="text" name="coverDate" class="form-control pull-right" value="{{ $paper->coverDate }}" id="datepicker">
                                </div>
                                <!-- /.input group -->
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Link trên trang khoa học</label>
                                <input type="text" class="form-control" name="url" placeholder="Đường dẫn..." value="{{ $paper->url }}" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">ID tạp chí</label>
                                <input type="text" class="form-control" name="issn" placeholder="ID tạp chí" value="{{ $paper->issn }}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Từ khóa</label>
                                <input type="text" class="form-control" name="keywords" placeholder="Từ khóa" value="{{ $paper->keywords }}">
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Abstract</label>
                                <textarea class="form-control" name="abstract">{{ $paper->abstract }}</textarea>
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('title'))
                                    <label for="exampleInputEmail1">{{ $errors->first('title') }}</label>
                                @endif
                            </div>
                        </div>
                        <!-- /.box-body -->
                    </div>
                    <!-- /.box -->
                    
                    <div class="box-footer">
                        <button type="submit" class="btn btn-primary">Thay đổi</button>
                    </div>
                </div>
            </form>
        </div>
    </section>
@endsection

