/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */
CKEDITOR.plugins.addExternal('symbol', 'plugins/symbol/', 'plugin.js');
CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	config.language = 'vi';
	// config.uiColor = '#AADC6E';
	   config.toolbar = 'MyToolbar';
	  
    config.toolbar_MyToolbar =

[
    ['Source','-','Save','NewPage','Preview','-'],
    ['Cut','Copy','Paste','PasteText','PasteFromWord','-'],
    ['Undo','Redo','-','Find','Replace','-','SelectAll','RemoveFormat'],
    ['Bold','Italic','Underline','Strike','-','Subscript','Superscript'],
    ['JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock'],
    '/',
    ['Styles','Format','FontSize'],
    ['NumberedList','BulletedList','-','Outdent','Indent','Blockquote'],
    ['Link','Unlink','Anchor'],
    ['Image','Flash','Table','HorizontalRule','Smiley','SpecialChar'],
    ['TextColor','BGColor'],
    
 

];

config.filebrowserBrowseUrl = "/kcfinder-master/browse.php";
config.filebrowserImageBrowseUrl = "/kcfinder-master/browse.php?type=images&dir=images/public";
config.filebrowserImageUploadUrl = "/kcfinder-master/browse.php?type=images&dir=images/public";
};
