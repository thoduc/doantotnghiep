@extends('layouts.app')


@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Tác giả
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="#">Danh sách tác giả</a></li>
        </ol>
    </section>
    <section class="content">
        <div class="row">
            <div class="col-xs-12">
                <div class="box">
                    <div class="box-header">
                        @if (Auth::check())
                        <a  href="{{ route('authors.create') }}"><button class="btn btn-primary">Thêm mới</button> </a>
                        <button class="btn btn-primary " onclick="return computeAuthorOffice(this);" data-loading-text="Cập nhật...">Cập nhật Phòng ban làm việc</button>
                        <div class="progress" style="display: none">
                            <div class="progress-bar" role="progressbar" aria-valuenow="0" aria-valuemin="0" aria-valuemax="100" >
                                0%
                            </div>
                        </div>
                        @endif
                    </div>
                    <!-- /.box-header -->
                    <div class="box-body">
                        <table id="authors" class="table table-bordered table-striped">
                            <thead>
                            <tr>
                                <th width="5%">ID</th>
                                <th width="10%">Tên </th>
                                <th width="10%">Email</th>
                                <th width="20%">Nơi làm việc</th>
                                <th width="20%">Lĩnh vực chuyên môn</th>
                                @if (Auth::check())
                                    <th>Thao tác</th>
                                @endif
                            </tr>
                            </thead>
                        </table>
                    </div>
                    <!-- /.box-body -->
                </div>
                <!-- /.box -->
            </div>
        </div>
    </section>
     @include('partials.popup_delete')
@endsection

@push('scripts')
<script>
    $(function() {
        $('#authors').DataTable({
            processing: true,
            serverSide: true,
            ajax: '{!! route('datatable_author') !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'title_show', name: 'givenName' },
                { data: 'email', name: 'email' },
                { data: 'affiliation', name: 'affiliation' },
                { data: 'subjects', name: 'subjects' },
                @if (Auth::check())
                { data: 'action', name: 'action', orderable: false, searchable: false },
                @endif
            ]
        });
    });

    function computeAuthorOffice(e) {
        var btn = $(e).button('loading');
        $('.progress').show();
        var start = 0;
        update();
        function update() {
            $.ajax({
                type: "get",
                url: '{!! route('computeAuthorOffice') !!}',
                data: {
                    start: start,
                },
                success: function(result){
                    var obj = jQuery.parseJSON( result);
                    console.log(start);
                    $('.progress-bar').attr('style', 'width: '+obj.percent+'%;');
                    $('.progress-bar').empty();
                    $('.progress-bar').append(obj.percent+'%');
                    start += 100;
                    if (obj.success == 1) {
                        $('.progress-bar').attr('style', 'width: 100%;');
                        $('.progress-bar').empty();
                        $('.progress-bar').append('100%');
                        alert('Bạn đã cập nhật thành công');
                        btn.button('reset')
                        $('.progress').hide();
                    } else {
                        update();
                    }
                }
            });
        }
    }
</script>
@endpush

