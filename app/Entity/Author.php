<?php

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'author';

    protected $primaryKey = 'author_id';

    protected $fillable = [
        'author_id',
        'id',
        'givenName',
        'surname',
        'email',
        'affiliation',
        'subjects',
        'url',
        'created_at',
        'updated_at'
    ];
}
