@extends('layouts.app')

@section('content')
    <!-- Content Header (Page header) -->
    <section class="content-header">
        <h1>
            Thêm mới tác giả
        </h1>
        <ol class="breadcrumb">
            <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
            <li><a href="{{ route('authors.index') }}">Tác giả</a></li>
            <li class="active">Thêm mới</li>
        </ol>
    </section>

    <section class="content">
        <div class="row">
            <!-- form start -->
            <form role="form" action="{{ route('authors.store') }}" method="POST">
                {!! csrf_field() !!}
                {{ method_field('POST') }}
                <div class="col-xs-12 col-md-12">

                    <!-- Nội dung thêm mới -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                            <h3 class="box-title">Nội dung</h3>
                        </div>
                        <!-- /.box-header -->

                        <div class="box-body">

                            <div class="form-group">
                                <label for="exampleInputEmail1">Mã tác giả</label>
                                <input type="text" class="form-control" name="id" placeholder="Mã tác giả" >
                            </div>
                            
                            <div class="form-group">
                                <label for="exampleInputEmail1">Trường/viện nơi họ làm việc</label>
                                <input type="text" class="form-control" name="affiliation" placeholder="Trường/viện nơi họ làm việc" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Lĩnh vực chuyên môn</label>
                                <input type="text" class="form-control" name="subjects" placeholder="Lĩnh vực chuyên môn" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Tên chính thức</label>
                                <input type="text" class="form-control" name="givenName" placeholder="Tên chính thức" required>
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Ký danh</label>
                                <input type="text" class="form-control" name="surname" placeholder="Ký danh" >
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <input type="email" class="form-control" name="email" placeholder="Email" required />
                            </div>

                            <div class="form-group">
                                <label for="exampleInputEmail1">Trang cá nhân</label>
                                <input type="text" class="form-control" name="url" placeholder="Trang cá nhân" >
                            </div>

                            <div class="form-group" style="color: red;">
                                @if ($errors->has('email'))
                                    <label for="exampleInputEmail1">{{ $errors->first('email') }}</label>
                                @endif
                                    @if ($errors->has('givenName'))
                                        <label for="exampleInputEmail1">{{ $errors->first('givenName') }}</label>
                                    @endif
                            </div>
                        </div>
                        <!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-primary">Thêm mới</button>
                        </div>
                    </div>
                    <!-- /.box -->
                </div>
                
            </form>
        </div>
    </section>
@endsection

