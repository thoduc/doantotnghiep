<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['prefix'=>''],function(){
    Route::get('/', 'HomeController@index')->name('home');
    Route::get('/home', 'HomeController@index')->name('home');
    Route::get('authors-customer', 'AuthorController@index')->name('authors.customer');
    Route::get('authors-show', 'AuthorController@anyDatatables')->name('datatable_author');
    
    Route::get('papers-customer', 'PaperController@index')->name('papers.customer');
    Route::get('papers-show', 'PaperController@anyDatatables')->name('datatable_papers');

    route::get('author-network', 'AuthorNetworkController@index')->name('author_network.index');
    route::get('author-network-customer', 'AuthorNetworkController@index')->name('author_network.customer');
    route::get('author-network/any-data', 'AuthorNetworkController@anyDatabase')->name('author_network.anyData');

    route::get('candidate', 'CandidateController@index')->name('candidate.index');
    route::get('candidate/any-data', 'CandidateController@anyDatabase')->name('candidate.anyData');

    route::get('candidate-customer', 'CandidateController@index')->name('candidate.customer');
});

Route::group(['prefix'=>'', 'middleware' => ['auth']],function(){
    Route::resource('authors', 'AuthorController');
    Route::get('authors-select', 'AuthorController@apiSelect')->name('select2_author');
    Route::get('compute-author-office', 'AuthorController@computeAuthorOffice')->name('computeAuthorOffice');

    Route::resource('papers', 'PaperController');
    Route::get('compute-keyword', 'PaperController@computeKeyword')->name('computeKeyword');

    Route::resource('users', 'UserController');

    route::get('author-network/update', 'AuthorNetworkController@computeNetwork')->name('author_network.update');
    
    route::get('candidate/update', 'CandidateController@computeCandidate')->name('candidate.update');
});



Route::group(['namespace'=>'Auth' ],function(){
    Route::get('login','LoginController@showLoginForm')->name('login');
    Route::post('login','LoginController@login');
    Route::get('logout','LoginController@logout');
    Route::post('logout','LoginController@logout')->name('logout');
    //reset password
    Route::get('password/reset','LoginController@getReset');
    Route::post('password/reset','LoginController@postReset');
});

