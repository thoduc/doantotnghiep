<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/12/2017
 * Time: 9:45 PM
 */

namespace App\Entity;
use Illuminate\Database\Eloquent\Model;

class AuthorPaper  extends Model
{
    protected $table = 'author_paper';

    protected $primaryKey = 'id';

    protected $fillable = [
        'id',
        'paperid',
        'authorid',
        'created_at',
        'updated_at'
    ];
}
