<?php
/**
 * Created by PhpStorm.
 * User: Nam Handsome
 * Date: 11/19/2017
 * Time: 2:14 PM
 */

namespace App\Entity;

use Illuminate\Database\Eloquent\Model;

class AuthorOffice extends Model
{
    protected $table = 'author_office';

    protected $primaryKey = 'author_office_id';

    protected $fillable = [
        'author_office_id',
        'author_id',
        'university',
        'department',
        'province',
        'country',
        'created_at',
        'updated_at',
    ];
}
